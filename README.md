# README #

Repository aplikasi web Laravel Base

### About ###

Aplikasi web menggunakan framework Laravel yang dapat digunakan sebagai rujukan atau basis pengembangan aplikasi web yang lain. Menggunakan framework Laravel 8.0, library [laravel-modular](https://github.com/dptsi/laravel-modular), library [laravel-sso](https://github.com/dptsi/laravel-sso), dan library [laravel-storage](https://github.com/dptsi/laravel-storage).

### Usage ###

Langkah-langkah untuk menjalankan aplikasi menggunakan docker.

> Bagi yang lebih suka visual bisa mengikuti [tutorial youtube](https://youtube.com/playlist?list=PLJ6oYK-zislCv2CSZ6fWsVgCkxQAZ95Wm)

Jika belum menjalankan nginx reverse proxy maka lakukan langkah-langkah berikut:

1. Pull github repository dptsi/nginx-proxy. 


	```bash
	
	git clone git@github.com:dptsi/nginx-proxy.git
	```
	
	
	atau 
	
	
	```bash
	
	git clone https://github.com/dptsi/nginx-proxy.git
	```

2. Buat docker network dengan nama nginx-proxy-network. 

	```bash
	docker network create --driver bridge nginx-proxy-network
	```

3. Jalankan docker compose di dalam direktori nginx-proxy. 

	```bash
	docker compose up -d
	```

Setelah nginx-proxy berjalan maka jalankan container laravel-base dengan menjalankan langkah-langkah berikut:

1. Copy file .env.sample menjadi .env di root folder

	```bash
	cp .env.sample .env
	```

2. Di root folder jalankan perintah berikut. 

	```bash
	docker compose up -d
	```

3. Tambahkan private IP address PC development dan domain laravel.local di /etc/hosts atau C:\Windows\System32\etc\hosts (pakai HostsMan).

8. Buka browser dan ketikkan laravel.local

Setelah container laravel-base berjalan, lakukan langkah-langkah berikut untuk setup instalasi laravel-base.

1. Buka docker terminal.

	```bash
	docker exec -it --user=root laravel-base-web /bin/sh
	```

2. Di folder /var/www/html/src lakukan perintah composer install untuk mengunduh seluruh library yang dibutuhkan ke folder vendor (opsional).

	```bash
	composer install
	```

3. Copy file .env.axample menjadi .env di folder src

	```bash
	cp .env.example .env
	```

4. Generate key aplikasi

	```bash
	php artisan key:generate
	```

5. Jalankan aplikasi via browser dan ketikkan https://laravel.local

### Troubleshoots ###

1. Jika mendapatkan exception UnexpectedValueException.

	```
	UnexpectedValueException
	The stream or file "/var/www/html/storage/logs/laravel.log" could not be opened in append mode: Failed to open stream: Permission denied 
	```

	maka masuk ke docker container sebagai root kemudian lakukan perintah sebagai berikut di folder /var/www/html.

	```bash
	chown -R nobody:nogroup storage
	```

2. Jika mendapatkan exception MissingAppKeyException.

	```
	Illuminate\Encryption\MissingAppKeyException
	No application encryption key has been specified. 
	```

	maka masuk ke docker container sebagai root kemudian lakukan perintah sebagai berikut di folder /var/www/html.

	```bash
	php artisan key:generate
	```

3. Jika IDE tidak dapat terkoneksi dengan Xdebug di container maka tambahkan konfigurasi berikut di docker-compose.yml

	```
	extra_hosts:
            - "host.docker.internal:host-gateway"
	```