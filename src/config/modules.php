<?php

return [

	'default_module' => 'beranda',
	'modules' => [
		'beranda' => [
			'module_class' => '\App\Modules\Beranda\Module',
			'enabled' => true,
		],
		'auth' => [
			'module_class' => '\App\Modules\Auth\Module',
			'enabled' => true,
		],
		'setting' => [
			'module_class' => '\App\Modules\Setting\Module',
			'enabled' => true,
		],
		'berkas' => [
			'module_class' => '\App\Modules\Berkas\Module',
			'enabled' => true,
		],
		'ui' => [
			'module_class' => '\App\Modules\Ui\Module',
			'enabled' => true,
		],
		'kafka' => [
			'module_class' => '\App\Modules\Kafka\Module',
			'enabled' => true,
		],
	],
];