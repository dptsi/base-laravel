<?php

return [
    'driver' => 'sqlsrv',
    'url' => env('AUTH_DATABASE_URL'),
    'host' => env('AUTH_DB_HOST', 'localhost'),
    'port' => env('AUTH_DB_PORT', '1433'),
    'database' => env('AUTH_DB_DATABASE', 'forge'),
    'username' => env('AUTH_DB_USERNAME', 'forge'),
    'password' => env('AUTH_DB_PASSWORD', ''),
    'charset' => 'utf8',
    'prefix' => '',
    'prefix_indexes' => true,
];