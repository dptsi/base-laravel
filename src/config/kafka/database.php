<?php

return [
    'driver' => 'sqlsrv',
    'url' => env('KAFKA_DATABASE_URL'),
    'host' => env('KAFKA_DB_HOST', 'localhost'),
    'port' => env('KAFKA_DB_PORT', '1433'),
    'database' => env('KAFKA_DB_DATABASE', 'forge'),
    'username' => env('KAFKA_DB_USERNAME', 'forge'),
    'password' => env('KAFKA_DB_PASSWORD', ''),
    'charset' => 'utf8',
    'prefix' => '',
    'prefix_indexes' => true,
];