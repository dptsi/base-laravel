@extends('errors::itserror')

@section('title', __('Server Error'))
@section('code', '500')
@section('image', url('assets/img/error-500.svg'))
@section('message', __($exception->getMessage() ?: 'Terjadi kesalahan pada sistem, lapor ke admin website.'))
