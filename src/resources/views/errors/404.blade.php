@extends('errors::itserror')

@section('title', __('Not Found'))
@section('code', '404')
@section('image', url('assets/img/error-404.svg'))
@section('message', __($exception->getMessage() ?: 'Halaman yang ada cari tidak bisa kami temukan, cek url.'))
