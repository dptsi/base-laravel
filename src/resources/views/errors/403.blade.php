@extends('errors::itserror')

@section('title', __('Forbidden'))
@section('code', '403')
@section('image', url('assets/img/error-403.svg'))
@section('message', __($exception->getMessage() ?: 'Anda tidak berhak mengakses halaman ini.'))
