<?php

namespace App\Handlers;

use Illuminate\Support\Facades\Log;
use Junges\Kafka\Contracts\KafkaConsumerMessage;
use App\Repositories\MessageBusError\MessageBusErrorRepository;
use App\Repositories\Pegawai\PegawaiRepository;
use App\Repositories\MessageBusError\MessageBusErrorRepositoryImpl;
use App\Repositories\Pegawai\PegawaiRepositoryImpl;

class KafkaConsumerHandler
{
    const ID_JENIS_SDM_DOSEN = 1;
    const ID_JENIS_SDM_TENDIK = 2;

    private PegawaiRepository $pegawaiRepository;
    private MessageBusErrorRepository $messageBusErrorRepository;

    public function __construct()
    {
        $this->pegawaiRepository = new PegawaiRepositoryImpl();
        $this->messageBusErrorRepository = new MessageBusErrorRepositoryImpl();
    }

    public function __invoke(KafkaConsumerMessage $message)
    {
        try {

            if($message->getTopicName()!=NULL)
            {
                var_dump($message->getBody());

                $info = $this->getMessageInfoConsuming($message);
                print($info);
                Log::channel("kafka")->debug($info);

                $data = $message->getBody();

                if
                (
                    $message->getTopicName()=='sdm' && 
                    $data['nama_event']=='sdm_updated' && 
                    ((int)$data['id_jenis_sdm']==self::ID_JENIS_SDM_DOSEN || (int)$data['id_jenis_sdm']==self::ID_JENIS_SDM_TENDIK)
                )
                {
                    $this->pegawaiRepository->insertOrUpdatePegawaiByMIHC($data);
                }
            }
        }
        catch(\Throwable $e)
        {
            print($e->getMessage());
            Log::error($e->getMessage());
            $this->messageBusErrorRepository->insert($message, $e->getMessage());
        }
    }

    public function getMessageInfoConsuming($message)
    {
        $info = sprintf("consuming from topic: %s, partition : %d, , offset :%d, data: %s\n", 
                $message->getTopicName(),
                $message->getPartition(),
                $message->getOffset(),
                json_encode($message->getBody()));

        return $info;
    }
}