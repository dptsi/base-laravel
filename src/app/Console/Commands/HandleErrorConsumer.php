<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Repositories\MessageBusError\MessageBusErrorRepository;
use App\Repositories\Pegawai\PegawaiRepository;
use App\Repositories\MessageBusError\MessageBusErrorRepositoryImpl;
use App\Repositories\Pegawai\PegawaiRepositoryImpl;

class HandleErrorConsumer extends Command
{
    protected $signature = 'kafka:handle-error-consumer';
    protected $description = 'Command description';

    private MessageBusErrorRepository $messageBusErrorRepo;
    private PegawaiRepository $pegawaiRepo;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->messageBusErrorRepo = new MessageBusErrorRepositoryImpl();
        $this->pegawaiRepo = new PegawaiRepositoryImpl();
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $allOfErrorData = $this->messageBusErrorRepo->getAllByTopicName(['data_keaktifan_simpeg']);

        foreach($allOfErrorData as $data)
        {
            $array_of_data = json_decode($data->payload, true);

            if
            (
                $data->topic=='sdm' &&
                ($array_of_data['nama_event']=='sdm_updated' || $array_of_data['nama_event']=='sdm_created')
            )
            {
                $pegawai = $this->pegawaiRepo->findById($array_of_data['id_sdm']);
               
                if($pegawai == NULL || ($array_of_data['updated_at'] > $pegawai->updated_at))
                {
                    $this->pegawaiRepo->insertOrUpdatePegawaiByMIHC($array_of_data);
                }
            }
        }
    }
}
