<?php

namespace App\Console\Commands;

use App\Handlers\KafkaConsumerHandler;
use Illuminate\Console\Command;
use Junges\Kafka\Facades\Kafka;
use Illuminate\Support\Facades\Log;
use Exception;
use App\Repositories\MessageBusError\MessageBusErrorRepositoryImpl;
use App\Repositories\Pegawai\PegawaiRepositoryImpl;

class KafkaConsumer extends Command
{
    protected $signature = 'kafka:consume';

    protected $description = 'Command description';

    const GROUP_ID = 'payroll-consumer-local-4';

    public function handle(): void
    {
        try {

            $consumer = Kafka::createConsumer()
            ->withBrokers(config('kafka.brokers'))
            ->subscribe(['sdm'])
            ->withHandler(new KafkaConsumerHandler(new PegawaiRepositoryImpl, new MessageBusErrorRepositoryImpl))
            ->withOption('auto.offset.reset', config('kafka.offset_reset'))
            // ->withMaxMessages(10)
            ->withConsumerGroupId(self::GROUP_ID)
            ->build();

            $consumer->consume();
        }
        catch(\Throwable $e)
        {
            print($e->getMessage());
            Log::channel('kafka')->debug($e->getMessage());
        }
    }
}