<?php

namespace App\Modules\Beranda;

use App\Modules\Ui\Contracts\BladeMenu;
use Dptsi\Modular\Base\BaseModule;

class Module extends BaseModule implements BladeMenu
{
    public function getProviders(): array
    {
        return [
            \App\Modules\Beranda\Providers\RouteServiceProvider::class,
            \App\Modules\Beranda\Providers\DatabaseServiceProvider::class,
            \App\Modules\Beranda\Providers\ViewServiceProvider::class,
            \App\Modules\Beranda\Providers\LangServiceProvider::class,
            \App\Modules\Beranda\Providers\BladeComponentServiceProvider::class,
            \App\Modules\Beranda\Providers\DependencyServiceProvider::class,
            \App\Modules\Beranda\Providers\EventServiceProvider::class,
            \App\Modules\Beranda\Providers\MessagingServiceProvider::class,
        ];
    }

    public function renderMenu(): string
    {
        return view("Beranda::menu")->render();
    }
}
