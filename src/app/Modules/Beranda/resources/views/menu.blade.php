<li class="nav-label mg-t-15">Beranda</li>
<li class="nav-item {{ (Request::is('/') || Request::is('beranda')) ? 'active' : '' }}">
    <a href="{{ url('/') }}" class="nav-link">
        <i data-feather="home"></i>
        <span>Beranda</span>
    </a>
</li>
