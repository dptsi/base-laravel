<?php

use Illuminate\Support\Facades\Route;
use App\Modules\Setting\Controllers\SettingController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/locale/{locale}', [SettingController::class, 'changeLocale'])->name('Setting::change.locale');

Route::get('/skin/{skin}', [SettingController::class, 'changeSkin'])->name('Setting::change.skin');
