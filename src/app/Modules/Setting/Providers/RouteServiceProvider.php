<?php

namespace App\Modules\Setting\Providers;

use App\Modules\Setting\Controllers\SettingController;
use Dptsi\Modular\Facade\Module;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    protected string $prefix = 'setting';
    protected string $module_name = 'setting';
    protected string $route_path = '../routes';

    public function boot()
    {
        $this->routes(function () {
            if (Module::get($this->module_name)->isDefault())
                 Route::middleware('web')->get('/', [SettingController::class, 'index']);

            Route::prefix('api/' . $this->prefix)
                ->middleware('api')
                ->group(__DIR__ . '/' . $this->route_path . '/api.php');

            Route::middleware('web')
                ->prefix("{$this->prefix}")
                ->group(__DIR__ . '/' . $this->route_path . '/web.php');
        });
    }
}
