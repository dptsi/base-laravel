<?php

namespace App\Modules\Kafka\Providers;

use Dptsi\Modular\Facade\Messaging;
use Illuminate\Support\ServiceProvider;

class MessagingServiceProvider extends ServiceProvider
{
    protected string $module_name = 'kafka';

    public function register()
    {
    }

    public function boot()
    {
        Messaging::setChannel('kafka');
//        Messaging::listenTo();
    }
}