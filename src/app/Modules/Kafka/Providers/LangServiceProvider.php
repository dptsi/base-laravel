<?php

namespace App\Modules\Kafka\Providers;

use Illuminate\Support\Facades\Lang;
use Illuminate\Support\ServiceProvider;

class LangServiceProvider extends ServiceProvider
{
    protected string $module_name = 'Kafka';
    protected string $lang_path = '../resources/lang';

    public function boot()
    {
        Lang::addNamespace($this->module_name, __DIR__ . '/' . $this->lang_path);
    }
}