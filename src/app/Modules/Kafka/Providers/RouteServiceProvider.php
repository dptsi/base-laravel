<?php

namespace App\Modules\Kafka\Providers;

use App\Modules\Kafka\Controllers\BaseController;
use Dptsi\Modular\Facade\Module;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    protected string $prefix = 'kafka';
    protected string $module_name = 'kafka';
    protected string $route_path = '../routes';

    public function boot()
    {
        $this->routes(function() {
            if (Module::get($this->module_name)->isDefault()) {
                Route::middleware('web')->get('/', [BaseController::class, 'index']);
            }

            Route::prefix('api/' . $this->prefix)
                ->middleware('api')
                ->group(__DIR__ . '/' . $this->route_path . '/api.php');

            Route::middleware('web')
                ->prefix("{$this->prefix}")
                ->group(__DIR__ . '/' . $this->route_path . '/web.php');
        });
    }
}