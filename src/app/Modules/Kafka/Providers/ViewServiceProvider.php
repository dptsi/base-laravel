<?php

namespace App\Modules\Kafka\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    protected string $module_name = 'Kafka';
    protected string $view_path = '../resources/views';

    public function boot()
    {
        View::addNamespace($this->module_name, __DIR__ . '/' . $this->view_path);
    }
}