<?php

namespace App\Modules\Kafka;

use Dptsi\Modular\Base\BaseModule;

class Module extends BaseModule
{
    public function getProviders(): array
    {
        return [
            \App\Modules\Kafka\Providers\RouteServiceProvider::class,
            \App\Modules\Kafka\Providers\DatabaseServiceProvider::class,
            \App\Modules\Kafka\Providers\ViewServiceProvider::class,
            \App\Modules\Kafka\Providers\LangServiceProvider::class,
            \App\Modules\Kafka\Providers\BladeComponentServiceProvider::class,
            \App\Modules\Kafka\Providers\DependencyServiceProvider::class,
            \App\Modules\Kafka\Providers\EventServiceProvider::class,
            \App\Modules\Kafka\Providers\MessagingServiceProvider::class,
        ];
    }
}