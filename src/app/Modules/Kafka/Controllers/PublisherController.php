<?php

namespace App\Modules\Kafka\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller;
use Junges\Kafka\Facades\Kafka;
use Junges\Kafka\Message\Message;
use Illuminate\Support\Facades\DB;

class PublisherController extends Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {
        $data = $this->getJson();
        $return_produce = $this->eventBaseProduce("tes_topic",$data);
        dd($return_produce);
    }

    public function getJson()
    {
        $data = json_decode(file_get_contents(__DIR__ . "/data.json"), true);
        return $data;
    }
    
    public function eventBaseProduce($topic, $payload)
    {
        $message = new Message(
            body: $payload
        );

        $producer = Kafka::publishOn($topic)->withMessage($message);
        $return = $producer->send();
        return $return;
    }

}
