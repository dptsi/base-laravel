<?php

namespace App\Modules\Ui;

use Dptsi\Modular\Base\BaseModule;

class Module extends BaseModule
{
    public function getProviders(): array
    {
        return [
            \App\Modules\Ui\Providers\RouteServiceProvider::class,
            \App\Modules\Ui\Providers\DatabaseServiceProvider::class,
            \App\Modules\Ui\Providers\ViewServiceProvider::class,
            \App\Modules\Ui\Providers\LangServiceProvider::class,
            \App\Modules\Ui\Providers\BladeComponentServiceProvider::class,
        ];
    }
}
