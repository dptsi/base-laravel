<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Other meta tags -->
        <meta name="description" content="Template yang digunakan untuk Sistem Informasi di ITS">
        <meta name="author" content="DPTSI ITS">

        <title>@yield('title') &bullet; myITS Template</title>

        <!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="{{ asset('assets/img/favicon.png') }}">

        <!-- vendor CSS -->
        <link href="{{ asset('lib/@fortawesome/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
        <link href="{{ asset('lib/ionicons/css/ionicons.min.css') }}" rel="stylesheet">
        <link href="{{ asset('lib/animate.css/animate.min.css') }}" rel="stylesheet">

        @yield('prestyles')

        <!-- DashForge CSS -->
        <link rel="stylesheet" href="{{ asset('assets/css/dashforge.min.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/dashforge.profile.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/dashforge.customs.css') }}">

        <style>
            @import url('https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap');
            #content {
                min-height: 66vh;
            }
            @media (min-width: 992px) {
                #content {
                    min-height: 69vh;
                }
            }
        </style>

        @yield('styles')

        @if (session('skin') == 'dark')
            <link rel="stylesheet" href="{{ asset('assets/css/skin.dark.css') }}">
        @else
            <link rel="stylesheet" href="{{ asset('assets/css/skin.light.css') }}">
        @endif
    </head>
    <body>
        <aside class="aside aside-fixed">
            @include('Ui::partials.menu')
        </aside>
        <div class="content ht-100v pd-0" style="position: relative">
            <div class="content-header align-items-center justify-content-start pos-fixed wd-100p z-index-10">
                <div>
                    @if (session('skin') == 'dark')
                        <a class="navbar-brand d-lg-none" href="{{ url('') }}">
                            <img src="{{ asset('assets/img/myits-template-white.png') }}" height="30" alt="" class="mg-r-15">
                        </a>
                    @else
                        <a class="navbar-brand d-lg-none" href="{{ url('') }}">
                            <img src="{{ asset('assets/img/myits-template-blue.png') }}" height="30" alt="" class="mg-r-15">
                        </a>
                    @endif
                </div>
                @include('Ui::partials.navbar-left')
                @include('Ui::partials.navbar-right')
            </div>
            <div class="content-body ht-100p pd-t-80">
                <div class="container pd-x-0" id="content">
                    <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
                        <div>
                            @yield('breadcrumbs')
                            <h4 class="mg-b-0 tx-montserrat tx-medium text-truncate">
                                @yield('header_title')
                            </h4>
                        </div>
                        <div class="d-lg-none mg-t-10">
                        </div>
                        <div>
                            @yield('header_right')
                        </div>
                    </div>

                    <div class="row row-xs">
                        @yield('content')
                    </div><!-- row -->
                </div><!-- container -->
                @include('Ui::partials.footer')
            </div>
        </div>

        @include('Ui::partials.role-modal')

        <script src="{{ asset('lib/jquery/jquery.min.js') }}"></script>
        <script src="{{ asset('lib/jqueryui/jquery-ui.min.js') }}"></script>
        <script src="{{ asset('lib/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ asset('lib/feather-icons/feather.min.js') }}"></script>
        <script src="{{ asset('lib/perfect-scrollbar/perfect-scrollbar.min.js') }}"></script>


        @yield('prescripts')

        <script src="{{ asset('assets/js/dashforge.js') }}"></script>
        <script src="{{ asset('assets/js/dashforge.aside.js') }}"></script>

        @yield('scripts')

        <script>
            $(document).ready(function($) {
                $(".custom-file-input").on("change", function() {
                    var fileName = $(this).val().split("\\").pop();
                    $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
                });
            });
        </script>

    </body>
</html>
