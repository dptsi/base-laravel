<div class="modal fade effect-scale" id="chgRoleUser" tabindex="-1" role="dialog" aria-labelledby="chgRoleUserLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content bg-white">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h5 class="tx-montserrat tx-medium" id="chgRoleUserLabel">{{ __('Ui::general.hak_akses') }}</h5>
                <p class="tx-color-02">{{ __('Ui::general.hak_akses_sekarang') }} <b>{{ sso()->user()->getActiveRole()->getName() }} {{ sso()->user()->getActiveRole()->getOrgName() ? sso()->user()->getActiveRole()->getOrgName() : '' }}</b>.</p>
                <form action="{{ route('Auth::change.role') }}" method="POST" class="form">
                    @csrf
                    <div class="form-group">
                        <select class="form-control" id="role" name="role">
                            <option selected>{{ __('Ui::general.pilih_hak_akses') }}</option>
                            @foreach (sso()->user()->getRoles() as $role)
                                <option value="{{ $role->getName() }}|{{ $role->getOrgName() ? $role->getOrgName() : '' }}">
                                    {{ $role->getName() }}
                                    @if($role->getOrgName())
                                        {{ $role->getOrgName() }}
                                        {{ $role->isActive()  ? '' : '('.__('Ui::general.tidak_aktif').')' }}
                                    @endif
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-its float-right tx-montserrat tx-semibold">{{ __('Ui::general.ganti') }}</button>
                        <button type="button" class="btn btn-white float-right tx-montserrat tx-semibold mg-r-10" data-dismiss="modal">{{ __('Ui::general.batal') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
