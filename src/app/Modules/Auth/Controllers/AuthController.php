<?php


namespace App\Modules\Auth\Controllers;


use Dptsi\Sso\Facade\Sso;
use Dptsi\Sso\Models\User;
use Dptsi\Sso\Requests\OidcLoginRequest;
use Dptsi\Sso\Requests\OidcLogoutRequest;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Its\Sso\OpenIDConnectClientException;

class AuthController extends Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function login()
    {
        try {
            $request = new OidcLoginRequest(
                config('openid.provider'),
                config('openid.client_id'),
                config('openid.client_secret'),
                config('openid.redirect_uri'),
                config('openid.scope'),
                config('openid.allowed_roles')
            );

            Sso::login($request);

            $this->updateOrInsertUser(Sso::user());

            return redirect()->to('/');
        } catch (OpenIDConnectClientException $e) {
            Session::remove('auth');
            Session::save();

            Log::error($e->getMessage());
        }
    }

    public function logout()
    {
        try {
            $request = new OidcLogoutRequest(
                config('openid.provider'),
                config('openid.client_id'),
                config('openid.client_secret'),
                config('openid.post_logout_redirect_uri')
            );

            Sso::logout($request);
        } catch (OpenIDConnectClientException $e) {
            Log::error($e->getMessage());
        }
    }

    public function changeRole(Request $request)
    {
        $roleName = explode('|', $request->role)[0];
        $orgName = explode('|', $request->role)[1];

        $user = Sso::user();

        foreach ($user->getRoles() as $role) {
            if ($role->getOrgName()) {
                if ($role->getName() === $roleName && $role->getOrgName() === $orgName && $role->isActive()) {
                    $user->setActiveRole($role);
                    break;
                } elseif ($role->getName() === $roleName && $role->getOrgName() === $orgName && !$role->isActive()) {
                    return redirect()->back()->with('success', __('Auth::general.hak_akses_tidak_aktif'));
                }
            } elseif ($roleName === $role->getName()) {
                $user->setActiveRole($role);
                break;
            }
        }

        Sso::set($user);

        return redirect()->back()->with('success', __('Auth::general.sukses_ubah_hak_akses'));
    }

    private function updateOrInsertUser(User $user)
    {
        DB::connection('auth')->table('user')
            ->updateOrInsert(
                ['id_user' => $user->getId()],
                [
                    'name' => $user->getName(),
                    'username' => $user->getUsername(),
                    'gender' => $user->getGender(),
                    'birthdate' => $user->getBirthdate(),
                    'email' => $user->getEmail(),
                    'email_verified' => $user->getEmailVerified(),
                    'alternate_email' => $user->getAlternateEmail(),
                    'alternate_email_verified' => $user->getAlternateEmailVerified(),
                    'phone' => $user->getPhone(),
                    'phone_verified' => $user->getPhoneVerified(),
                    'zoneinfo' => $user->getZoneinfo(),
                    'locale' => $user->getLocale(),
                    'picture' => $user->getPicture(),
                    'created_at' => now(),
                    'updated_at' => now(),
                    'updater' => $user->getId(),
                ]
            );
    }
}
