<?php

use Illuminate\Support\Facades\Route;
use App\Modules\Berkas\Controllers\BerkasController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::name('Berkas::')->group(function () {
    Route::get('/', [BerkasController::class, 'index'] );
    Route::post('tambah', [BerkasController::class, 'store'])->name('store');
    Route::post('sunting', [BerkasController::class, 'update'])->name('update');
    Route::post('hapus', [BerkasController::class, 'delete'])->name('delete');
    Route::get('{file_name}', [BerkasController::class, 'stream'])->name('stream');
});
