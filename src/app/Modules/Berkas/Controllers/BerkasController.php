<?php

namespace App\Modules\Berkas\Controllers;

use App\Modules\Berkas\Requests\StoreBerkasRequest;
use App\Modules\Berkas\Requests\UpdateBerkasRequest;
use Dptsi\FileStorage\Facade\FileStorage;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Ramsey\Uuid\Uuid;

class BerkasController extends Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {
        $jenisDokumen = DB::connection('berkas')->table('ref.jenis_dokumen')->get();

        return view('Berkas::index', compact('jenisDokumen'));
    }

    public function jenisDokumen(Request $request)
    {
        if($request->has('id_jenis_dok'))
            $result = DB::connection('berkas')->table('ref.jenis_dokumen')->where('id_jenis_dok', $request->id_jenis_dok)->first();

        if($result)
            return response()->json($result);

        return response()->json(['error' => 'Data tidak ditemukan'], 404);
    }

    public function berkas(Request $request)
    {
        if($request->has('id_jenis_dok') && $request->has('nama_file')) {
            $result = DB::connection('berkas')->table('dbo.dokumen')
                ->join('ref.jenis_dokumen', 'dbo.dokumen.id_jenis_dok', '=', 'ref.jenis_dokumen.id_jenis_dok')
                ->where('dbo.dokumen.id_user', sso()->user()->getId())
                ->select(
                    'ref.jenis_dokumen.nama as jenis_dokumen',
                    'dbo.dokumen.id_dokumen',
                    'dbo.dokumen.id_jenis_dok',
                    'dbo.dokumen.nama_file',
                    'dbo.dokumen.ekstensi',
                    'dbo.dokumen.keterangan',
                )
                ->orderBy('dbo.dokumen.id_jenis_dok')
                ->orderBy('dbo.dokumen.nama_file');
            if($request->id_jenis_dok != '' && $request->nama_file != '') {
                $result = $result
                    ->where('dbo.dokumen.nama_file', 'like', '%' . $request->nama_file . '%')
                    ->where('dbo.dokumen.id_jenis_dok', $request->id_jenis_dok)
                    ->get();
            } elseif($request->nama_file != '') {
                $result = $result
                    ->where('dbo.dokumen.nama_file', 'like', '%' . $request->nama_file . '%')
                    ->get();
            } elseif($request->id_jenis_dok != '') {
                $result = $result
                    ->where('dbo.dokumen.id_jenis_dok', $request->id_jenis_dok)
                    ->get();
            } else {
                $result = $result
                    ->get();
            }

        } elseif ($request->has('id_dokumen')) {
            $result = DB::connection('berkas')->table('dbo.dokumen')
                ->join('ref.jenis_dokumen', 'dbo.dokumen.id_jenis_dok', '=', 'ref.jenis_dokumen.id_jenis_dok')
                ->where('dbo.dokumen.id_dokumen', $request->id_dokumen)
                ->select(
                    'ref.jenis_dokumen.nama as jenis_dokumen',
                    'dbo.dokumen.nama_file',
                    'dbo.dokumen.ekstensi',
                    'dbo.dokumen.ukuran',
                    'dbo.dokumen.keterangan',
                    'dbo.dokumen.created_at',
                    'dbo.dokumen.updated_at',
                )
                ->first();
        }
        if($result)
            return response()->json($result);

        return response()->json(['error' => 'Data tidak ditemukan'], 404);
    }

    public function stream(Request $request)
    {
        try {
            $dokumen = DB::connection('berkas')->table('dbo.dokumen')->where('id_dokumen', $request->id_dokumen)->first();

            if($dokumen) {
                file_put_contents($dokumen->nama_file, file_get_contents(config('filestorage.host_uri') . $dokumen->public_link));
                if($request->aksi == 'detail') {
                    return response()->file($dokumen->nama_file)->deleteFileAfterSend(true);
                }
                if($request->aksi == 'unduh') {
                    return response()->download($dokumen->nama_file, $dokumen->nama_file .  '.' . $dokumen->ekstensi)->deleteFileAfterSend(true);
                }
            }

            $request->session()->flash('error', 'Berkas tidak ditemukan.');
            return redirect()->back();
        } catch (\Exception $e) {
            Log::error($e->getMessage());

            $request->session()->flash('error', 'Terjadi kesalahan saat mengambil berkas.');
            return redirect()->back();
        }
    }

    public function store(StoreBerkasRequest $request)
    {
        try {
            $response = FileStorage::upload($request->file('berkas'));

            if($response->status == FileStorage::statusSuccess()) {
                $data = [
                    'id_dokumen'    => Uuid::uuid4()->toString(),
                    'id_jenis_dok'  => $request->id_jenis_dok,
                    'id_user'       => sso()->user()->getId(),
                    'nama_file'     => $request->nama_file,
                    'mime'          => $response->info->file_mimetype,
                    'ekstensi'      => $response->info->file_ext,
                    'keterangan'    => $request->keterangan,
                    'ukuran'        => $response->info->file_size,
                    'file_id'       => $response->info->file_id,
                    'public_link'   => $response->info->public_link,
                    'created_at'    => now(),
                    'updated_at'    => now(),
                    'updater'       => sso()->user()->getId(),
                ];
                DB::connection('berkas')->table('dbo.dokumen')->insert($data);

                return response()->json(['success' => 'Berkas berhasil diunggah.']);
            }

            return response()->json(['error' => 'Berkas gagal diunggah.']);
        } catch (\Exception $e) {
            Log::error($e->getMessage());

            return response()->json(['error' => 'Terjadi kesalahan.']);
        }
    }

    public function update(UpdateBerkasRequest $request)
    {
        try {
            // lebih baik gunakan Model, ini hanya sebagai contoh
            $query = DB::connection('berkas')->table('dbo.dokumen')->where('id_dokumen', $request->id_dokumen);
            $dokumen = (clone $query)->first();

            if(!$dokumen)
                return response()->json(['error' => 'Berkas tidak ditemukan.']);

            if($dokumen->id_user != sso()->user()->getId())
                return response()->json(['error' => 'Anda tidak berhak memperbarui berkas ini.']);

            $data = [
                'nama_file'     => $request->nama_file,
                'keterangan'    => $request->keterangan,
                'updated_at'    => now(),
                'updater'       => sso()->user()->getId(),
            ];

            $query->update($data);

            return response()->json(['success' => 'Berkas berhasil diperbarui.']);
        } catch (\Exception $e) {
            Log::error($e->getMessage());

            return response()->json(['error' => 'Terjadi kesalahan.']);
        }
    }

    public function delete(Request $request)
    {
        try {
            // lebih baik gunakan Model, ini hanya sebagai contoh
            $query = DB::connection('berkas')->table('dbo.dokumen')->where('id_dokumen', $request->id_dokumen);
            $dokumen = (clone $query)->first();

            if(!$dokumen)
                return response()->json(['error' => 'Berkas tidak ditemukan.']);

            if($dokumen->id_user != sso()->user()->getId())
                return response()->json(['error' => 'Anda tidak berhak menghapus berkas ini.']);

            FileStorage::delete($dokumen->file_id);

            $query->delete();

            return response()->json(['success' => 'Berkas berhasil dihapus.']);
        } catch (\Exception $e) {
            Log::error($e->getMessage());

            return response()->json(['error' => 'Terjadi kesalahan.']);
        }
    }
}
