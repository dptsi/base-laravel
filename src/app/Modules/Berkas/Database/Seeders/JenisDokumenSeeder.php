<?php

namespace App\Modules\Berkas\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class JenisDokumenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $payload = [
            [
                'nama' => 'Sertifikat',
                'nama_en' => 'Certificate',
                'tipe_file' => 'pdf',
                'ukuran_file_min' => 0,
                'ukuran_file_maks' => 1500,
            ],
            [
                'nama' => 'Foto',
                'nama_en' => 'Photos',
                'tipe_file' => 'jpg,jpeg,png',
                'ukuran_file_min' => 0,
                'ukuran_file_maks' => 2000,
            ],
        ];

        foreach ($payload as $p) {
            DB::connection('berkas')->table('ref.jenis_dokumen')->insert($p);
        }
    }
}
