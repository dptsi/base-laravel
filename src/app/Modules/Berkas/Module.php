<?php

namespace App\Modules\Berkas;

use App\Modules\Ui\Contracts\BladeMenu;
use Dptsi\Modular\Base\BaseModule;

class Module extends BaseModule implements BladeMenu
{
    public function getProviders(): array
    {
        return [
            \App\Modules\Berkas\Providers\RouteServiceProvider::class,
            \App\Modules\Berkas\Providers\DatabaseServiceProvider::class,
            \App\Modules\Berkas\Providers\ViewServiceProvider::class,
            \App\Modules\Berkas\Providers\LangServiceProvider::class,
            \App\Modules\Berkas\Providers\BladeComponentServiceProvider::class,
            \App\Modules\Berkas\Providers\DependencyServiceProvider::class,
            \App\Modules\Berkas\Providers\EventServiceProvider::class,
            \App\Modules\Berkas\Providers\MessagingServiceProvider::class,
        ];
    }

    public function renderMenu(): string
    {
        return view("Berkas::menu")->render();
    }
}
