<?php

namespace App\Modules\Berkas\Providers;

use Dptsi\Modular\Facade\Messaging;
use Illuminate\Support\ServiceProvider;

class MessagingServiceProvider extends ServiceProvider
{
    protected string $module_name = 'berkas';

    public function register()
    {
    }

    public function boot()
    {
        Messaging::setChannel('berkas');
//        Messaging::listenTo();
    }
}