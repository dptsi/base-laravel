<?php

namespace App\Modules\Berkas\Providers;

use App\Modules\Berkas\Controllers\BerkasController;
use Dptsi\Modular\Facade\Module;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    protected string $prefix = 'berkas';
    protected string $module_name = 'berkas';
    protected string $route_path = '../routes';

    public function boot()
    {
        $this->routes(function() {
            if (Module::get($this->module_name)->isDefault()) {
                Route::middleware('web')->get('/', [BerkasController::class, 'index']);
            }

            Route::middleware('web')
                ->prefix('api/' . $this->prefix)
                ->group(__DIR__ . '/' . $this->route_path . '/api.php');

            Route::middleware('web')
                ->prefix("{$this->prefix}")
                ->group(__DIR__ . '/' . $this->route_path . '/web.php');
        });
    }
}
