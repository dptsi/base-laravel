<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Repositories\MessageBusError\MessageBusErrorRepository;
use App\Repositories\MessageBusError\MessageBusErrorRepositoryImpl;
use App\Repositories\Pegawai\PegawaiRepository;
use App\Repositories\Pegawai\PegawaiRepositoryImpl;

class DependencyServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(MessageBusErrorRepository::class, MessageBusErrorRepositoryImpl::class);
        $this->app->bind(PegawaiRepository::class, PegawaiRepositoryImpl::class);
    }
}
