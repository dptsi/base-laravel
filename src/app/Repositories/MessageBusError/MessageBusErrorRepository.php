<?php 

namespace App\Repositories\MessageBusError;

use Junges\Kafka\Message\ConsumedMessage;
use Illuminate\Support\Collection;

interface MessageBusErrorRepository
{
    public function insert(ConsumedMessage $message, string $errorMessage): void;
    public function getAll(): Collection;
    public function getAllByTopicName(array $topic_name): Collection;
    public function getMessageSuksesInsertErrorData(array $arrayOfData): string;
}