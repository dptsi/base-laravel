<?php 

namespace App\Repositories\MessageBusError;

use App\Repositories\MessageBusError\MessageBusErrorRepository;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;
use Junges\Kafka\Message\ConsumedMessage;
use Carbon\Carbon;

class MessageBusErrorRepositoryImpl implements MessageBusErrorRepository
{

    public function insert(ConsumedMessage $message, string $errorMessage): void
    {
        $arrayOfData = [
            'id_event'=>Uuid::uuid4()->toString(),
            'payload'=>json_encode($message->getBody()),
            'topic'=>$message->getTopicName(),
            'partition'=>$message->getPartition(),
            'offset'=>$message->getOffset(),
            'error_message'=>$errorMessage,
            'timestamp_kafka'=>Carbon::createFromTimestampMsUTC($message->getTimestamp())->setTimezone('Asia/Jakarta'),
            'created_at'=>now(),
            'updated_at'=>now()
        ];

        DB::connection('message_bus_error')->table('dbo.consumer_example')
        ->insert($arrayOfData);

        print($this->getMessageSuksesInsertErrorData($arrayOfData));
        Log::channel("kafka")->debug($this->getMessageSuksesInsertErrorData($arrayOfData));
    }

    public function getAll(): Collection
    {
        $data = DB::connection('message_bus_error')
            ->table('dbo.consumer_example')
            ->whereNull('deleted_at')->get();
        
        return $data;
    }

    public function getAllByTopicName(array $topic_name): Collection
    { 
        $data = DB::connection('message_bus_error')
            ->table('dbo.consumer_example')
            ->whereIn('topic', $topic_name)
            ->whereNull('deleted_at')
            ->orderBy('created_at', 'asc')
            ->orderBy('partition', 'asc')
            ->orderBy('offset', 'asc')
            ->get();
        
        return $data;
    }

    public function getMessageSuksesInsertErrorData(array $arrayOfData): string
    {
        $message = sprintf(
            "Sukses menginsertkan data yg error, data => %s\n", json_encode($arrayOfData)
        );   

        return $message;
    }
}