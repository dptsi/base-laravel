<?php 

namespace App\Repositories\Pegawai;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Repositories\Pegawai\PegawaiRepository;

class PegawaiRepositoryImpl implements PegawaiRepository
{
    public function insertOrUpdatePegawaiByMIHC(array $array): void
    {
        DB::table('dbo.pegawai')->updateOrInsert(
            [
                'id_pegawai'=>$array['id_sdm']
            ],
            [
                'id_pegawai'=>$array['id_sdm'],
                'id_user_sso'=>$array['id_user'],
                'nama'=>$array['nama']
            ]
        );

        print($this->getMessageSuksesUpdateSdm($array));
        Log::channel("kafka")->debug($this->getMessageSuksesUpdateSdm($array));
    }

    public function findById(string $idPegawai)
    {
        $array = DB::table('dbo.pegawai')->where('id_pegawai', $idPegawai)->first();

        return $array;
    }

    public function getMessageSuksesUpdateSdm(array $array): string
    {
        $message = sprintf(
            "Berhasil mengupdate data pegawai. data yg diupdate=> %s\n", json_encode($array)
        );

        return $message;
    }
}