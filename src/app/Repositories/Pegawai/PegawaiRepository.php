<?php 

namespace App\Repositories\Pegawai;


interface PegawaiRepository
{
    public function insertOrUpdatePegawaiByMIHC(array $array): void;
    public function getMessageSuksesUpdateSdm(array $array): string;
    public function findById(string $idPegawai);
}